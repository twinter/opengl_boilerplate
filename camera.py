# Copyright (C) 2016 by the opengl_boilerplate contributors,
# see the git history and README.md for details.
# This file is part of opengl_boilerplate.
#
# opengl_boilerplate is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# opengl_boilerplate is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with opengl_boilerplate in agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.


from constants import *
from helpers import *
import world

# import pyglet
from pyglet.gl import *
from pyglet.window import *

# import builtins
from typing import TypeVar
import numpy as np
import time


class Camera(pyglet.window.Window):
	def __init__(self, world: TypeVar('world.World'), width=1024, height=768, *args, **kwargs):
		super(Camera, self).__init__(resizable=True, caption=WINDOW_TITLE, *args, **kwargs)

		self.world = world

		# position and orientation (x, y, z) relative to the world
		# x: to the right
		# y: upwards
		# z: forward
		self.position = self.world.get_middle(height=25)
		self.orientation = (40.0, 180.0, 0.0)

		# the speed and acceleration in direction of the x and z axes
		self.speed = (0.0, 0.0, 0.0)
		self.acceleration = (0.0, 0.0, 0.0)

		# movement orders relative to the camera
		# first element: 1 right, -1 left; second element: +up -down; third element: +backwards, -forward
		self.movement = [0, 0, 0]

		# the following two are for time controlled height changes
		self.move_target_time = 0
		self.move_target_height = self.position[1]

		self.orientation_target = self.orientation
		self.orientation_target_time = 0

		# if True: reports mouse position and locks the mouse to this window
		# no hidden property for fullscreen as it exists in pyglet.window.Window
		self._exclusive = False
		self.exclusive = property(lambda self: self._exclusive, self.set_exclusive_mouse)

		self.windowed = property(lambda self: not self.fullscreen, self.set_windowed)

		self.setup_opengl()
		self.setup_fog()

		# The label that is displayed in the top left of the canvas.
		self.label1 = pyglet.text.Label('', font_name='Arial', font_size=12, x=10, y=self.height - 10, anchor_x='left',
		                                anchor_y='top', color=(0, 0, 0, 255))
		self.label2 = pyglet.text.Label('', font_name='Arial', font_size=12, x=10, y=20, anchor_x='left',
		                                anchor_y='bottom', color=(0, 0, 0, 255))

		pyglet.clock.schedule_interval(self.update, 1.0 / CLOCK_TICKS_PER_SECOND)

		list2 = pyglet.graphics.vertex_list(4, ('v3i', (5, 2, 5, 5, 2, 0, 0, 2, 0, 0, 2, 5)),
		                                    ('c3B', (128, 128, 128) * 4))
		list2.draw(GL_QUADS)

		pyglet.app.run()

	def setup_opengl(self):
		self.set_exclusive_mouse(self._exclusive)
		self.set_fullscreen(self._fullscreen)

		# Set the color of "clear", i.e. the sky, in rgba.
		glClearColor(0.9, 0.9, 0.9, 1)

		# Enable culling (not rendering) of back-facing facets -- facets that aren't
		# visible to you.
		if ENABLE_CULLING:
			glEnable(GL_CULL_FACE)

		# Set the texture minification/magnification function to GL_NEAREST (nearest
		# in Manhattan distance) to the specified texture coordinates. GL_NEAREST
		# "is generally faster than GL_LINEAR, but it can produce textured images
		# with sharper edges because the transition between texture elements is not
		# as smooth."
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)

	def setup_fog(self):
		"""
			copied entirely from that Minecraft clone for now
		"""

		# Enable fog. Fog "blends a fog color with each rasterized pixel fragment's
		# post-texturing color."
		glEnable(GL_FOG)
		# Set the fog color.
		glFogfv(GL_FOG_COLOR, (GLfloat * 4)(0.5, 0.69, 1.0, 0.8))
		# Say we have no preference between rendering speed and quality.
		glHint(GL_FOG_HINT, GL_DONT_CARE)
		# Specify the equation used to compute the blending factor.
		glFogi(GL_FOG_MODE, GL_LINEAR)
		# How close and far away fog starts and ends. The closer the start and end,
		# the denser the fog in the fog range.
		glFogf(GL_FOG_START, 20.0)
		glFogf(GL_FOG_END, 60.0)

	# --- EVENTS ---
	def on_resize(self, width, height):
		# TODO: put all the loading in here (especially the placement of the label)
		pass

	def on_draw(self):
		pyglet.clock.tick()

		# reset
		self.clear()
		glColor3d(1, 1, 1)

		# draw 3D stuff (world)
		self.set3d()
		self.world.draw()

		# draw 2D stuff (gui)
		self.set2d()
		px, py, pz = self.position

		vx, vy, vz = self.speed
		ox, oy, oz = self.orientation
		self.label1.text = u'{:02f} (p: {:.2f}, {:.2f}, {:.2f}; v: {:.2f}, {:.2f}, {:.2f}; o: {:.2f}, {:.2f} {:.2f})' \
			.format(pyglet.clock.get_fps(), px, py, pz, vx, vy, vz, ox, oy, oz)
		self.label1.draw()
		mx, my, mz = self.movement
		ax, ay, az = self.acceleration
		self.label2.text = u'm: {:.2f} {:.2f} {:.2f}; a: {:.2f} {:.2f} {:.2f}'.format(mx, my, mz, ax, ay, az)
		self.label2.draw()

	def on_key_press(self, symbol, modifier):
		if symbol == key.F:
			print('key: F')
			self.windowed = not self.windowed
			print('windowed:', self.windowed)
		elif symbol == key.W:
			self.movement[2] -= 1
		elif symbol == key.S:
			self.movement[2] += 1
		elif symbol == key.A:
			self.movement[0] -= 1
		elif symbol == key.D:
			self.movement[0] += 1
		elif symbol == key.Q:
			self.movement[1] -= 1
		elif symbol == key.E:
			self.movement[1] += 1
		elif symbol == key.UP:
			self.change_orientation(-1 * ORIENTATION_CHANGE_ARROWS, 0, smooth=True)
		elif symbol == key.DOWN:
			self.change_orientation(ORIENTATION_CHANGE_ARROWS, 0, smooth=True)
		elif symbol == key.RIGHT:
			self.change_orientation(0, ORIENTATION_CHANGE_ARROWS, smooth=True)
		elif symbol == key.LEFT:
			self.change_orientation(0, -1 * ORIENTATION_CHANGE_ARROWS, smooth=True)
		elif symbol == key.ESCAPE:
			quit()

	def on_key_release(self, symbol, modifier):
		if symbol == key.W:
			self.movement[2] -= -1
		elif symbol == key.S:
			self.movement[2] += -1
		elif symbol == key.A:
			self.movement[0] -= -1
		elif symbol == key.D:
			self.movement[0] += -1
		elif symbol == key.Q:
			self.movement[1] -= -1
		elif symbol == key.E:
			self.movement[1] += -1

	def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
		if buttons & pyglet.window.mouse.MIDDLE:
			self.change_orientation(-dy * ORIENTATION_CHANGE_MOUSE, dx * ORIENTATION_CHANGE_MOUSE)

	# --- HELPERS ---
	def update(self, dt):
		"""
			called repeatedly by pyglet.clock
			triggers updates for camera position and the environment
		"""

		dt = min(dt, UPDATE_TIME_LIMIT)

		# update world
		self.world.update(dt)
		self.update_speed(dt)
		self.update_position(dt)
		self.update_orientation(dt)

	def update_speed(self, dt):
		# v = a * dt + v0
		# rotate self.speed to match the camera direction
		v0 = self.rotate_vector(self.speed, -1 * self.orientation[1])

		# calculate acceleration from self.movement relative to the camera
		a = np.multiply((self.movement[0], self.movement[1], self.movement[2]), MOVE_ACCELERATION)

		# calculate new speed (vn)
		vn = np.multiply(a, dt)
		vn = np.add(vn, v0)

		# check if a is 0 on any axis, decelerate on this axis if so
		for i in range(len(a)):
			if a[i] == 0:
				vn[i] += -1 * sgn(vn[i]) * dt * MOVE_DECELERATION
				if sgn(v0[i]) + sgn(vn[i]) == 0:
					vn[i] = 0.0

		# limit speed to terminal velocity
		if np.linalg.norm(vn) > MOVE_TERMINAL_VELOCITY:
			l = np.linalg.norm(vn)
			vn = np.divide(vn, l)
			vn = np.multiply(vn, MOVE_TERMINAL_VELOCITY)

		# convert speed back relative to the world
		self.acceleration = self.rotate_vector(a, self.orientation[1])
		self.speed = self.rotate_vector(vn, self.orientation[1])

	def update_position(self, dt):
		# p = a / 2 * dt**2 + v0 * dt + p0
		p0 = self.position
		p = np.divide(self.acceleration, 2)
		p = np.multiply(p, dt ** 2)
		p2 = np.multiply(self.speed, dt)
		p = np.add(p, p2)
		p = np.add(p, p0)
		self.position = p

	def update_orientation(self, dt):
		"""
		do a smooth transition to the target orientation
		:param dt:
		:return:
		"""
		time_remain = self.orientation_target_time - time.time()
		if time_remain > 0:
			o_remain = np.subtract(self.orientation_target, self.orientation)
			do = np.multiply(o_remain, dt / time_remain)
			if np.linalg.norm(o_remain) < np.linalg.norm(do):
				do = o_remain
			self.orientation = np.add(do, self.orientation)
		else:
			self.orientation = self.orientation_target

	def set_exclusive_mouse(self, exclusive: bool) -> None:
		self._exclusive = exclusive
		super().set_exclusive_mouse(exclusive)

	def set_windowed(self, windowed: bool) -> None:
		print('debug')
		fs = not windowed
		super().set_fullscreen(fullscreen=fs)

	def set2d(self):
		width, height = self.get_size()
		glDisable(GL_DEPTH_TEST)
		glViewport(0, 0, width, height)
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		glOrtho(0, width, 0, height, -1, 1)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()

	def set3d(self):
		width, height = self.get_size()
		glEnable(GL_DEPTH_TEST)
		glViewport(0, 0, width, height)
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(90.0, width / float(height), 0.1, 1000.0)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()

		# rotation w/ glRotatef(angle, x, y, z) (rotate a certain angle around the given vector)
		x, y, z = self.orientation
		glRotatef(x, 1, 0, 0)
		glRotatef(y, 0, 1, 0)
		glRotatef(z, 0, 0, 1)

		# translate the camera to it's position
		x, y, z = self.position
		glTranslatef(-x, -y, -z)

	def rotate_vector(self, v, angle: float, in_radians=False):
		"""
		rotate a 2d or 3d vector by a given angle

		:param v: 2-tupel or 3-tupel (float) containing the vector
		:param angle: float
		:param in_radians: bool
		:return:
		"""
		if not in_radians:
			angle = angle * np.pi / 180.0

		try:
			if len(v) == 2:
				x, y = v
			elif len(v) == 3:
				x, z, y = v
			x2 = x * np.cos(angle) - y * np.sin(angle)
			y2 = x * np.sin(angle) + y * np.cos(angle)
			if len(v) == 2:
				return np.array([x2, y2])
			elif len(v) == 3:
				return np.array([x2, v[1], y2])
		except:
			raise TypeError('no rotatable vector given')

	def change_orientation(self, amount_x, amount_y, amount_z=0, smooth=False):
		if smooth:
			if time.time() > self.orientation_target_time:
				self.orientation_target = self.orientation
			self.orientation_target = np.add(self.orientation_target, (amount_x, amount_y, amount_z))
			self.orientation_target_time = time.time() + ORIENTATION_CHANGE_TIME
		else:
			self.orientation_target = np.add(self.orientation_target, (amount_x, amount_y, amount_z))
