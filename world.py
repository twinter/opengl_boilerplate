# Copyright (C) 2016 by the opengl_boilerplate contributors,
# see the git history and README.md for details.
# This file is part of opengl_boilerplate.
#
# opengl_boilerplate is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# opengl_boilerplate is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with opengl_boilerplate in agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.


# python std lib
from typing import Tuple

# other libs
from pyglet.gl import *
import numpy as np

# own modules
from constants import *


class World:
	"""
	This class abstracts the OpenGL world. It's sole purpose is to display the information the server generates.
	It is supposed to manage and draw all entities in the OpenGl world space but is far from done at the moment.
	"""

	def __init__(self, size: Tuple[float, float]):
		self.size = size  # size of the grid drawn on the floor

		self._floor_line_strips = None
		self._make_floor_grid()


	# called by on_draw()
	def draw(self):
		# self._draw_test()

		for x in self._floor_line_strips:
			x.draw(GL_LINE_STRIP)

	# called repeatedly, shouldn't take too long to run. meant for world updates.
	def update(self, dt):
		pass

	def get_middle(self, height=15):
		return (self.size[0] / 2.0, height, self.size[1] / 2.0)

	def _make_floor_grid(self):
		"""
		fill self.floor_line_strips with the vertices of the floor grid.
		coordinated are >= 0
		"""
		# TODO: use indexed batch/list/etc
		# NOTE: batch does seem to connect different lists of shared types
		# TODO: use height

		self._floor_line_strips = []

		x = 0
		inc_x = FLOOR_GRID_SIZE
		inc_y = inc_x * np.sqrt(3.0 / 4.0)
		last_col = None
		last_col_c = None
		odd_row = False

		color = (0, 0, 0)

		while x <= self.size[0]:
			# build a column
			col = tuple()
			col_c = tuple()

			y = 0
			if odd_row:
				col += (x, 0, y)
				col_c += color
				y += 0.5 * inc_y

			while y <= self.size[1]:
				col += (x, 0, y)
				col_c += color
				y += inc_y

			if y - 0.5 * inc_y <= self.size[1]:
				col += (x, 0, y - 0.5 * inc_y)
				col_c += color

			# add lines along the z axis
			self._floor_line_strips.append(pyglet.graphics.vertex_list(int(len(col) / 3), ('v3f', col), ('c3b', col_c)))

			# make a vertex lists and add them to the batch
			if last_col is not None:
				positions = self._merge_vertex_lists(col, last_col)
				colors = self._merge_vertex_lists(col_c, last_col_c)

				# add degenerate vertices
				positions = self._add_degenerate_vertices(positions)
				colors = self._add_degenerate_vertices(colors)

				amount = int(len(positions) / 3)

				self._floor_line_strips.append(pyglet.graphics.vertex_list(amount, ('v3f', positions), ('c3B', colors)))

			last_col = col
			last_col_c = col_c
			x += inc_x
			odd_row = not odd_row

	def _merge_vertex_lists(self, a, b, group_size=3):
		# start with the list with the smaller y distance between vertex 0 and 1
		if a[5] - a[2] > b[5] - b[2]:
			a, b = b, a
		a = tuple(a)
		b = tuple(b)
		ret = tuple()
		for i in range(0, len(a), group_size):
			ret += a[i: i + group_size]
			ret += b[i: i + group_size]  # no need to check for index out of bounds as x[a:b:c] returns () if so
		return ret

	def _add_degenerate_vertices(self, x, dimension=3):
		return x[0:dimension] + x + x[-dimension:]

	def _draw_test(self, h=1):
		for x in range(-50, 51, 25):
			for y in range(-50, 51, 25):
				vertex_list = pyglet.graphics.vertex_list(6,
				                                          ('v3i',
				                                           (10 + x, h, 10 + y, 10 + x, h, 0 + y, 0 + x, h, 10 + y,
				                                            -10 + x, h, -10 + y, -10 + x, h, 0 + y, 0 + x, h, -10 + y)),
				                                          ('c3B', (255, 0, 0, 0, 255, 0, 0, 0, 255) * 2))
				vertex_list.draw(GL_TRIANGLES)
		list2 = pyglet.graphics.vertex_list(4, ('v3i', (5, 2, 5, 5, 2, 0, 0, 2, 0, 0, 2, 5)),
		                                    ('c3B', (255, 255, 255) * 4))
		list2.draw(GL_QUADS)

# --------- dumped code under this --------------

		# def _make_floor_grid
		'''
		self._floor_batch = pyglet.graphics.Batch()

		x = 0
		inc_x = float(self.config.client.floor_grid_size)
		inc_y = inc_x * np.sqrt(3.0 / 4.0)  # every side of every triangle has the same length (without height)
		last_col = None
		last_col_c = None
		odd_row = False

		color = (128, 128, 128)

		while x <= self.size[0]:
			# build a column
			col = tuple()
			col_c = tuple()

			y = 0
			if odd_row:
				col += (x, self.game.getHeight((x, y)), y)
				col_c += color
				y += 0.5 * inc_y

			while y <= self.size[1]:
				col += (x, self.game.getHeight((x, y)), y)
				col_c += color
				y += inc_y

			if y - 0.5 * inc_y <= self.size[1]:
				y -= 0.5 * inc_y
				col += (x, self.game.getHeight((x, y)), y)
				col_c += color

			# make a vertex lists and add them to the batch
			if last_col is not None:
				positions = self._merge_vertex_lists(last_col, col)
				colors = self._merge_vertex_lists(last_col_c, col_c)

				# add degenerate vertices
				positions = self._add_degenerate_vertices(positions)
				colors = self._add_degenerate_vertices(colors)

				amount = int(len(positions) / 3)

				second_color = (0, 0, 0) * amount  # TODO: get edge color to work

				if odd_row:
					render_group = self._render_groups['reverse_face']
				else:
					render_group = None
				# FIXME face reversal not working for large y values (starting at size_y=2772)
				# could it be that batch only takes a certain amount of vertices?
				self._floor_batch.add(amount, GL_TRIANGLE_STRIP, render_group, ('v3f', positions), ('c3B', colors),
				                      ('s3B', second_color))

			last_col = col
			last_col_c = col_c
			x += inc_x
			odd_row = not odd_row
		'''
		
		# def _merge_vertex_lists
		'''
		# FIXME: the switching of the lists causes the direction of the triangle_strip to reverse because op the changed
			order of the vertices. the lines marked with FIX should to the trick but are completely untested.
		
		# start with the list with the smaller y (3rd position per 3d vertex) distance between vertex 0 and 1
		reverse = False  # FIX
		if a[2 * vertex_length - 1] - a[vertex_length - 1] > b[2 * vertex_length - 1] - b[vertex_length - 1]:
			a, b = b, a  # a should have the same length as b or be one vertex longer
			reverse = True  # FIX
		ret = tuple()
		for i in range(0, len(b), vertex_length):
			ret += a[i: i + vertex_length]
			ret += b[i: i + vertex_length]
		for i in range(len(b), len(a), vertex_length):
			ret += a[i: i + vertex_length]
		
		if reverse:  # FIX
			return list(reversed(ret))  # FIX
		else:  # FIX
			return ret
		'''