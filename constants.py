# Copyright (C) 2016 by the opengl_boilerplate contributors,
# see the git history and README.md for details.
# This file is part of opengl_boilerplate.
#
# opengl_boilerplate is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# opengl_boilerplate is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with opengl_boilerplate in agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.


WINDOW_TITLE = 'Kriegsspiel'

# limits the maximum time difference used to calculate updates in seconds
UPDATE_TIME_LIMIT = 0.2

# world updates per second
CLOCK_TICKS_PER_SECOND = 60

# distance between points on the floor grid
FLOOR_GRID_SIZE = 2.5

ENABLE_CULLING = True


MOVE_TERMINAL_VELOCITY = 25

MOVE_ACCELERATION = 50

MOVE_DECELERATION = 50

MOVE_HEIGHT_CHANGE_TIME = 0.5

MOVE_HEIGHT_CHANGE_DISTANCE = 10

ORIENTATION_CHANGE_MOUSE = 0.5

ORIENTATION_CHANGE_ARROWS = 10

ORIENTATION_CHANGE_TIME = 0.5
