# Copyright (C) 2016 by the opengl_boilerplate contributors,
# see the git history and README.md for details.
# This file is part of opengl_boilerplate.
#
# opengl_boilerplate is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# opengl_boilerplate is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with opengl_boilerplate in agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.


"""
this is supposed to be the entry point into the program.
this function calls everything else needed and could be used for other logic
"""

import world
import camera


if __name__ == '__main__':
	w = world.World((1000, 1000))
	c = camera.Camera(w)
