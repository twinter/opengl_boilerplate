# OpenGl Boilerplate #

### Basics ###
* this is basically a simple OpenGl Boilerplate based on pyglet
* you need python 3.5, pyglet and numpy to run
* UTF-8 encoding is used on all files

### Developement Status ###
* Camera works for most cases quite well
* World only displays a simple test pattern and parts of the floor grid
* World will be handling the placement and management of all entities in the OpenGL world

### Controls ###
* wasd to move horizontally
* q and e to change the height
* mouse drag or arrows keys to look around

### Contributers ###
* the contributers of kriegsspiel, see repos from [catpig](https://gitlab.com/catpig/kriegsspiel) and  [twinter](https://gitlab.com/twinter/kriegsspiel)
* [Tom Winter](https://gitlab.com/u/twinter)

### LICENSE ###
opengl_boilerplate if licensed under AGPL 3.0. See agpl_3.0.txt for details